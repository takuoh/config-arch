.PHONY: etc

pkglist: pkglist.txt
	pacman -Qqe | sort > pkglist.txt

CMD_paru := cd /tmp && git clone https://aur.archlinux.org/paru-bin.git && \
	    cd paru-bin && makepkg -s && \
	    doas pacman -U paru-bin* && \
	    doas pacman -Rsn --noconfirm paru-bin-debug

paru: check_git check_basedevel
	@until $(CMD_paru); do \
		sleep 0.1 ; \
	done

CMD_sys := doas cp -r etc/ / && \
    echo -e "tmpfs\t/home/$(USER)/.cache\ttmpfs\tnoatime,nodev,nosuid,size=2G\t0\t0\ntmpfs\t/tmp\ttmpfs\trw,nodev,nosuid\t0\t0" | doas tee -a /etc/fstab && \
	doas chmod 700 /boot /etc/{iptables,nftables.conf} || true && \
	doas chmod -c 0400 /etc/doas.conf

etc:
	@until $(CMD_sys) ; do \
		sleep 0.1; \
	done

unlock_user:
	faillock --user $(USER) --reset

CMD_libvirt := doas pacman -Sy virt-manager --noconfirm --needed && \
	       doas gpasswd -a $(USER) libvirt

libvirt:
	@until $(CMD_libvirt) ; do \
		sleep 0.1; \
	done

dot: check_rsync
	rsync -av dotfiles/. ~

install_pkg: check_paru
	paru -Sy --sudo doas --needed $(shell grep -v '^#' pkglist.txt)

chsh: check_zsh
	@until chsh -s /bin/zsh; do \
		sleep 0.1; \
	done

rust: check_rustup
	rustup install stable

usbguard: check_usbguard
	@until doas sh -c 'usbguard generate-policy -PH > /etc/usbguard/rules.conf'; do \
		sleep 0.1; \
	done

ff:
	killall firefox; rm -rf ~/.mozilla ; cp -r dotfiles/.mozilla ~ ; rm -rf ~/.cache/.* ~/.cache/* /tmp/* /tmp/.* 

wireshark: check_wireshark
	@until doas gpasswd -a $(USER) wireshark; do \
		sleep 0.1; \
	done
CMD_archcn := curl -o archcn.pkg.tar.zst https://repo.archlinuxcn.org/x86_64/archlinuxcn-keyring-20220716-1-any.pkg.tar.zst && \
	      doas pacman --noconfirm -U archcn.pkg.tar.zst && \
	      rm archcn.pkg.tar.zst

archcn:
	@until $(CMD_archcn); do \
		sleep 0.1; \
	done

CMD_black := curl -O https://www.blackarch.org/keyring/blackarch-keyring.pkg.tar.xz && \
	     doas pacman --noconfirm -U blackarch-keyring.pkg.tar.xz && \
	     rm blackarch-keyring.pkg.tar.xz && \
	     doas pacman-key --lsign-key F9A6E68A711354D84A9B91637533BAFE69A25079

blackarch:
	@until $(CMD_black); do \
		sleep 0.1;\
	done

gpg:
	find ~/.local/share/gnupg -type f -exec chmod 600 {} \;
	find ~/.local/share/gnupg -type d -exec chmod 700 {} \;

edict:
	curl -L 'https://raw.github.com/skk-dev/dict/master/SKK-JISYO.edict' -o ~/.local/share/fcitx5/skk/SKK-JISYO.edict

yaskkserv2: check_yaskkserv2
	yaskkserv2_make_dictionary /usr/share/skk/SKK-JISYO.L ~/.local/share/fcitx5/skk/SKK-JISYO.edict --dictionary-filename=$(HOME)/.local/share/fcitx5/skk/dictionary.yaskkserv2

TMP := $(shell mktemp -d)
CMD_gkr := cd docs && \
	   cp gnome-keyring-pam.patch $(TMP) && \
	   cd $(TMP) && \
	   cp /etc/pam.d/login . &&\
	   patch -i gnome-keyring-pam.patch login && \
	   doas cp login /etc/pam.d/login

gkr_pam:
	@until $(CMD_gkr); do \
		sleep 0.1; \
	done

SYSTEMD_sys := dnscrypt-proxy.service avahi-daemon.service bluetooth cups docker firewalld rc-local tailscaled thermald tlp libvirtd.socket usbguard root-resume

systemd_sys:
	@until doas systemctl enable $(SYSTEMD_sys); do \
		sleep 0.1; \
	done

SYSTEMD_user := gcr-ssh-agent.socket gnome-keyring-daemon.service syncthing yaskkserv2

systemd_user: 
	systemctl enable --user $(SYSTEMD_user)


GIT := $(shell command -v git 2> /dev/null)
check_git:
ifndef GIT
	$(error "install git please.")
endif

RSYNC := $(shell command -v rsync 2> /dev/null)
check_rsync:
ifndef RSYNC
	$(error "install rsync please.")
endif

PARU := $(shell command -v paru 2> /dev/null)
check_paru:
ifndef PARU
	$(error "install paru please.")
endif

ZSH := $(shell command -v zsh 2> /dev/null)
check_zsh:
ifndef ZSH
	$(error "install zsh please.")
endif

RUSTUP := $(shell command -v rustup 2> /dev/null)
check_rustup:
ifndef RUSTUP
	$(error "install rustup please.")
endif

USBGUARD := $(shell command -v usbguard 2> /dev/null)
check_usbguard:
ifndef USBGUARD
	$(error "install usbguard please.")
endif

WIRESHARK := $(shell command -v wireshark 2> /dev/null)
check_wireshark:
ifndef WIRESHARK
	$(error "install wireshark-qt please.")
endif

YASKKSERV2 := $(shell command -v yaskkserv2 2> /dev/null)
check_yaskkserv2:
ifndef YASKKSERV2 
	$(error "install yaskkserv2-bin please.")
endif

BASEDEVEL := $(shell pacman -Q base-devel 2> /dev/null)
check_basedevel:
ifndef BASEDEVEL
	$(error "install base-devel please.")
endif

