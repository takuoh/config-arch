# dotfiles and /etc tree

```bash
git clone https://gitlab.com/takuoh/config-arch.git --recurse
```

# Setup with make

```bash
doas pacman -Sy base-devel git --needed
make blackarch
make paru
make libvirt
make etc
make install_pkg | make rust
# install custom pkg from takuoh/PKGBUILDs
make dot
make chsh
make usbguard
make wireshark
make gpg
make edict
make yaskkserv2
make gkr_pam
make systemd_sys
make systemd_user
make ff
```

# Symlink nvim config

```bash
rm -rf ~/.config/nvim/
cd dotfiles/.config/
ln -rsf nvim ../../../../../../.config
```
