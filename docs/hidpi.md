# DPI and scale

- 96dpi = 1.0

- 144dpi = 1.5

- 物理DPI: 1920px / 293mm * 25.4mm/inch = 166dot/inch

# Mouse Pointer

swayの場合マウスカーソルもスケールするので小さいサイズのカーソルを設定

- [sway 24](https://gitlab.com/takuoh/config-arch/-/blob/daa808ba54b7890662f989731fa1a806b6c3008c/dotfiles/.config/sway/config#L327)

- [X11 32](https://gitlab.com/takuoh/config-arch/-/blob/57b5d808f0d4f9c6b9a7c7b443bbadacc3528174/dotfiles/.config/X11/xresources#L1)

- [cinnamon 32](https://gitlab.com/takuoh/config-arch/-/blob/2646ae5b877c59503e175d9e527762043f4b728d/docs/cinnamon.txt#L21)

~~- [gtk 32](https://gitlab.com/takuoh/config-arch/-/blob/3626b3a117398b717633b67fd35d2a43fc136221/dotfiles/.config/gtk-3.0/settings.ini#L6)~~

# X11

## KDE

KScreenを使う

不要な変数はenvの-uオプションでunsetさせる

https://gitlab.com/takuoh/config-arch/-/blob/d15dc67d6a6705cad2da307a7a598c763f6457d5/dotfiles/.xinitrc#L45 

kdeを起動する際に、QT_AUTO_SCREEN_SCALE_FACTORはなぜか0にセットされる

https://invent.kde.org/plasma/plasma-workspace/-/blob/4d3768485f56a6b344d023d07ed772c6827aed9e/startkde/startplasma.cpp#L351

## GTK3

thunar, nemoなどのGTK3アプリケーションはフォントのみスケール

firefox, chromium, electronはUIもスケール

https://gitlab.com/takuoh/config-arch/-/blob/de57c89d4387e12b445c83423333d769939c92db/dotfiles/.config/X11/profile#L7

## Qt

~~QT_SCREEN_SCALE_FACTORSはスクリーンごとにスケール設定できる。UIのみをスケール。~~

~~QT_FONT_DPIで強制的にdpiを設定可能(deprecated)。~~

QT_AUTO_SCREEN_SCALE_FACTORを1に設定することで完璧(物理スクリーンDPI)。

https://gitlab.com/takuoh/config-arch/-/blob/de57c89d4387e12b445c83423333d769939c92db/dotfiles/.config/X11/profile#L8

## alacritty(winit)

物理スクリーンDPIを使用

https://gitlab.com/takuoh/config-arch/-/blob/3530e253e1aed90b8bcf0eb286ed35bfed15b269/dotfiles/.config/X11/profile#L12

## rofi

物理スクリーンDPIを使用

https://gitlab.com/takuoh/config-arch/-/blob/3530e253e1aed90b8bcf0eb286ed35bfed15b269/dotfiles/.config/rofi/config.rasi#L14

# WAYLAND

## Sway

GTKの場合、小数スケールすると1.5の場合、2倍スケールした後0.75倍で縮小スケールしているので無駄にリソースを使う。

waydroid, flameshotでおかしくなる。[flameshot issue#1705](https://github.com/flameshot-org/flameshot/issues/1705)

https://gitlab.com/takuoh/config-arch/-/blob/daa808ba54b7890662f989731fa1a806b6c3008c/dotfiles/.config/sway/config#L323

## foot

https://gitlab.com/takuoh/config-arch/-/blob/d15dc67d6a6705cad2da307a7a598c763f6457d5/dotfiles/.config/foot/foot.ini#L23

# Misc

## Console

terminus-fontをインストール、設定

https://gitlab.com/takuoh/config-arch/-/blob/0f3827d57eca2871ecd498ec66aa07c57a6c6575/etc/vconsole.conf#L1

# memo

## 考え

UIとフォント両方スケールする設定はしない。(GDK_DPI_SCALE)

フォントのみスケーリングできる場合は高dpiにあわせる(フォント, マウスポインター)

結論:X11でのマルチモニター, 完全なスケーリングは諦めよう

### GTK3

GTK3のスケール環境変数"GDK_DPI_SCALE"はマルチモニターにやさしくない。いちいち設定しなおす必要がある

けどgsettingsでtext-scaling-factorを設定するよりはマシ(環境変数を変更するほうが楽)

### Qt

QT_FONT_DPIでフォントサイズ調整
なぜかこの変数でchromiumがUIフォント共に、スケーリングされるようになっていた。GDK_DPI_SCALEと挙動は同じ。

### java

マルチモニターは_JAVA_OPTIONS書き変え

https://gitlab.com/takuoh/config-arch/-/blob/d15dc67d6a6705cad2da307a7a598c763f6457d5/dotfiles/.config/zsh/.zshrc#L70
