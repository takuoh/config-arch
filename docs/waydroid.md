# setup

```bash
doas waydroid init -s GAPPS
```

# clean

```bash
doas \rm -rf /var/lib/waydroid /home/.waydroid ~/waydroid ~/.share/waydroid ~/.local/share/applications/*aydroid* ~/.local/share/waydroid
```
